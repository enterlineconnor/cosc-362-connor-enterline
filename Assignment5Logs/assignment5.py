import re
import glob
import subprocess

count = 0

for count in list(glob.glob('*.txt')):

	print(" ")
	rofile = open(count,'r')
	file = rofile.read()
	numbers = re.findall(r"[-+]?\d*\.\d+|\d+", file)
	total = 0
	line = open(count).readline()
	print(line+" - "),
	for i in range(len(numbers)):
		if int(i) % 2 == 0:
			 val = int(numbers[int(i)]) * 60
		 	 total += val
		else:
			 total += int(numbers[int(i)])

	print(str(total) + " Minutes")
	print("OR")
	print(str(total/60) + " Hours")
	print(" ")
	
