#!/bin/bash

echo '1.) Names of All tex Files'
echo '---------------------------'
echo ''
find . -name "*.tex"


echo '2.) Converting All tex Files to PDFs'
echo '-------------------------------------'
echo ''
pdflatex *.tex

echo '3.) Removing All aux, log, and bib Files'
echo '-----------------------------------------'
echo ''
rm -rf *.aux
rm -rf *.log
rm -rf *.bib

echo '4.) Saves Word Count And Name To File'
echo '--------------------------------------'
echo ''
wc -w *.tex > LatexCompileReport.txt
echo 'LatexCompileReport.tex Updated!'
