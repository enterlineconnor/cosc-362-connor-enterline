/* The header file */ 

/* Function to see if the value of the pixel is in the circle or not */ 
int InCircle(int totalrows, int totalcols, int radius, int pixRow, int pixCols);

/* Function to see if the location requires a hole to make eyes, a nose, or mouth. */
int holes(int totalrows, int totalcols, int radius, int pixRow, int pixCols);


/* Function to see if the location requires a stem */
int stem(int totalrows, int totalcols, int radius, int pixRow, int pixCols);
