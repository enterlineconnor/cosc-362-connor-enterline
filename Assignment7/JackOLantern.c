#include <stdio.h>
#include <stdlib.h>
#include "MyFunctions7.h"

/* 
 *   This is a c program to create a Jack O Lantern
 *   Usage: 
 * 
 *   PurpleCircle OutFileName s
 * 
 */ 

int main(int argc,    // This is the number of things passed into this function.  
         char *argv[] // This is the array of things passed. 
        ){

    int numRows = 1000;    /* Set number of rows    */ 
    int numCols = 1000;    /* Set number of columns */ 
    int imageSize;  /* Total number of pixels we will use     */ 
    int row, col;   /* row, and column counters               */
    int radius  = 200;     /* Set radius of Pumpkin                      */
    int InOut;      /* Flag where (0 = out , and 1 = circle)  */  
    int holes;      /* Flag where (0 = no hole, and 1 = hole  */
    int top;	    /* Flag where (0 = no stem, and 1 = stem  */
    unsigned char *outImage;  /* pixel pointer                */ 
    unsigned char *ptr;       /* a ponter                     */ 
    //unsigned char *outputFP;  /* Output file                  */ 
    FILE *outputFP;           

    

    if(argc!=2){
        printf("Usage: ./JackOLantern OUTfileName \n");
        exit(1);
    }

    printf("=====================================\n");  
    printf("I'm making a Jack O Lantern        \n");
    printf("=====================================\n\n");

   

    // ============================================
    // Set up space for my soon to be ppm image. 
    // ============================================
    imageSize = numRows*numCols*3;  
    outImage  = (unsigned char *) malloc(imageSize); // get enough space for my image. 

    /* Open a file to put the output image into */ 
    if((outputFP = fopen(argv[1], "w")) == NULL){
        perror("output open error");
        printf("Error: can not open output file\n");
        exit(1);
    } 
    
    /* Now lets create the plain pixel map! */ 
    ptr = outImage; 



    for(row = 0; row < numRows; row++){
        for(col = 0; col < numCols; col++){
            // Walk through each row of the image column by column. 
            // Use a function to decide if you are in the circle or not.
            InOut = InCircle(numRows,numCols,radius,row,col);
	    holes = cuts(numRows,numCols,radius,row,col);
	    top   = stem(numRows,numCols,radius,row,col);

            if (InOut ==1){
               /* Orange */ 
               *ptr     = 255;
               *(ptr+1) = 165; 
               *(ptr+2) = 0;
		if(holes ==1){
		/* Yellow */
		    *ptr     = 255;
		    *(ptr+1) = 255;
	            *(ptr+2) = 0;
		}

	}

            if (top ==1){

		*ptr   = 38;
		*(ptr+1) = 106;
		*(ptr+2) = 46;

	   }
            if(top != 1 && InOut != 1) {
               /* Black Pixel */ 
               *ptr     = 0; 
               *(ptr+1) = 0; 
               *(ptr+2) = 0; 
            }


            // Advance the pointer. 
            ptr += 3; 
        }
    }


    // Put all of this information into a file with a need header. 
    fprintf(outputFP, "P6 %d %d 255\n", numCols, numRows);
    fwrite(outImage, 1, imageSize, outputFP);

    /* Done */ 
    fclose(outputFP);

    return 0;
}

